#!/bin/bash

##############################################################################
# Config 
##############################################################################

# set variables
file=Terminal.desktop
file_path=/usr/share/applications
patch=Terminal.patch
patch_path=./

##############################################################################
# Script
##############################################################################

# must be run as root
if [ $(whoami) != "root" ]; then
	echo "[ee]: script must be run as root"
	exit 1
fi

# check if Terminal is already installed
yum list installed | grep '^Terminal\.' > /dev/null 2>&1

# install Terminal if it is not already installed
if [ $? -ne 0 ]; then
	yum -y install Terminal
fi

# check if a backup file already exists
if [ -f "$file_path/$file.bak" ]; then
	# exit with an error if it does
	echo "[ee]: backup file already exists"
	exit 1
fi

# create the backup file
cp "$file_path/$file" "$file_path/$file.bak"

# patch the original file
echo "Patching $file_path/$file"
sed -i '/^Exec/n;/^Categories/n;s/=\(.*Terminal.*\)/=XFCE \1/' "$file_path/$file"

# check if patch succeeded
if [ $? -ne 0 ]; then
	echo "[ee]: Patch failed"
	exit 1
fi

exit 0
