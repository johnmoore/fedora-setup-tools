#!/usr/bin/perl -w
use strict;

binmode(STDOUT, ":utf8");

sub usage {
  print "\n";
  print "**************************\n";
  print "*XTERM 256Color Test Chart\n";
  print "**************************\n";
  print "*\n";
  print "* Usage:\n";
  print "* colortest -e\n";
  print "*    extended (256) display\n";
  print "*\n";
  print "* colortest -h\n";
  print "*    help\n";
  print "*\n";
  print "* colortest -w\n";
  print "*    wide display\n";
  print "*\n";
  print "* colortest -x\n";
  print "*    hex display\n";
  print "*\n";
  print "* colortest -v\n";
  print "*    verbose display\n";
  print "*\n";
  print "* colortest -r\n";
  print "*    display reversed\n";
  print "*\n";
  print "* colortest -t\n";
  print "*    display tabular\n";
  print "*\n";
  print "* colortest -b\n";
  print "*    display blocks\n";
  print "*\n";
  print "**************************\n";
  print "\n";
}


my $ascii_colors_per_line     = 1;
my $ascii_colors_256_per_line = 1;
my $rows_per_block            = 6;
my $display_wide              = 0;
my $display_hex               = 0;
my $display_reverse           = 0;
my $display_verbose           = 0;
my $display_tabular           = 0;
my $display_extended          = 0;
my $display_blocks            = 0;
my @ascii_color_name_table    = ();
my @ascii_color_hex_table     = ();


my %args                      = map { $_ => 1 } @ARGV;

my $padding_left  = "";
my $padding_right = "";

if(exists($args{"-b"})) {
  $display_blocks = 1;
}
if(exists($args{"-w"})) {
  $display_wide = 1;
}
if(exists($args{"-e"})) {
  $display_extended = 1;
}
if(exists($args{"-x"})) {
  #$ascii_colors_per_line = 12;
  $display_hex = 1;
}
if(exists($args{"-x"})) {
  $display_hex = 1;
}
if(exists($args{"-r"})) {
  $display_reverse = 1;
}
if(exists($args{"-t"})) {
  $display_tabular = 1;
}
if(exists($args{"-v"})) {
  $display_verbose = 1;
}
if(exists($args{"-h"})) {
  usage();
  exit(0);
}

sub get_column_count {
  my $columns = 1;
  if ($display_tabular == 1) {
    $columns = 8;
  }
  return $columns;
}

sub get_column_padding_left_width {
  my $width = 1;
  if ($display_wide == 1) {
    $width = 2;
  }
  return $width;
}

sub get_column_padding_right_width {
  my $width = 1;
  if ($display_wide == 1) {
    $width = 2;
  }
  return $width;
}

sub render_ascii_color_value {
  my $out                 = "";
  my $width               = 0;
  my $total_width         = 0;
  my $padding_left_width  = get_column_padding_left_width();
  my $padding_right_width = get_column_padding_right_width();

  $width       = get_column_width();
  $total_width = get_total_column_width();

  $out = sprintf("%*s%*s%*s", $padding_left_width, $padding_left, $width, $_[0], $padding_right_width, $padding_right);
  return sprintf("%*s", $total_width, $out);
}

sub render_ascii_color {
  my $color = $_[0];
  # get the proper format 
  my $out   = "";

  $out = format_ascii_color($color);
  # render the value 
  $out = render_ascii_color_value($out);
  # add color! 
  $out = color_string($color, $out);
  return $out;
}

sub color_string_get_contrast {
  my $r = hex($_[0]);
  my $g = hex($_[1]);
  my $b = hex($_[2]);

  my $luminance = 0;
  # standard, objective
  #my $luminance = 1 - ( 0.2126 * $r + 0.7152 * $g + 0.0722 * $b)/255;
  $luminance = ( 0.2126 * $r + 0.7152 * $g + 0.0722 * $b);
  # perceived 1 
  #my $luminance = 1 - ( 0.299 * $r + 0.587 * $g + 0.114 * $b)/255;
  # perceived 2 
  #my $luminance = ( (0.241 * $r)*(0.241 * $r) + (0.691 * $g)*(0.691 * $g) + (0.068 * $b)*(0.068 * $b) ) / 255;
  # "good enough" 
  my $baseline = 255 - (($r + $r + $g + $g + $b + $b)/6);

  my $hex = sprintf("%x", $luminance);

  my @hex_values = (hex("00"), hex("5f"), hex("87"), hex("af"), hex("d7"), hex("ff"));
  my @dec_values = (0, 59, 102, 145, 188, 255);

  my $a_l = 0;
  my $idx = 0;

  foreach(@hex_values) {
    if ($luminance >= $_) {
      $idx++;
    }
  }
  if ( $idx >= 5 ) {
    $a_l = $dec_values[5];
  }
  elsif (($hex_values[$idx + 1] - $luminance) < ($luminance - $hex_values[$idx])) {
    $a_l = $dec_values[$idx + 1];
  }
  else {
    $a_l = $dec_values[$idx];
  }

  #print "test approx ($luminance) -> $_[0]/$_[1]/$_[2] [$idx]: " . $a_l;

  if ($a_l < 145) {
    return 255;
  } else {
    return 0;
  }

  return $a_l;


}

sub color_string {
  my $color = $_[0];
  my $original_color = $_[0];
  my $string = $_[1];
  my $original_string = $_[1];
  my $out = "";
  my $prefix="";

  if ($display_blocks == 1 && $display_hex == 1) {
    #$string =~ s/^\s+//;
    #$string =~ s/\s+$//;
    my $offset_left = get_column_padding_left_width();
    my $offset_right = get_column_padding_right_width();
    my $len = length($string) - $offset_left - $offset_right;
#print "len: $len ol: $offset_left or: $offset_right\n";
    $string = substr $string, $offset_left, $len;
#print "str: **$string**\n";

    if ($display_verbose == 1) {
      $prefix = substr $string, 0, 6;
      $string = substr $string, 6;
      $prefix = color_string_foreground($original_color, $prefix);
      #print "prefix: **$prefix** og: **$original_string** str: **$string**\n";
      $original_string = substr($original_string, 6);
      $string = $ascii_color_hex_table[$color];
      #print "prefix: **$prefix** og: **$original_string** str: **$string**\n";
      $original_string = sprintf("%*s%s%*s", $offset_left, " ", $string, $offset_right, " ");
    }

    $color = color_string_get_contrast(substr($string, 0, 2), substr($string, 2, 2), substr($string, 4, 2));

    $original_string = color_string_background($original_color, $original_string);
  }
  $out = color_string_foreground($color, $original_string);
  return $prefix . $out;
}

sub color_string_background {
  my $color  = $_[0];
  my $string = $_[1];
  return sprintf("\e[48;5;%dm%s\e[0m", $color, $string);
}

sub color_string_foreground {
  my $color  = $_[0];
  my $string = $_[1];
  return sprintf("\e[38;5;%dm%s\e[0m", $color, $string);
}

sub render_ascii_color_name {
  my $color = $_[0];
  my $name  = "";
  if (($display_verbose == 1 && $display_tabular == 1)) {
    $name = $ascii_color_name_table[$color]{"long"};
  }
  else {
    $name = $ascii_color_name_table[$color]{"short"};
  }
  $name = render_ascii_color_value($name);
  return $name;
}

sub format_ascii_color_block {
  my $color = $_[0];
  my $out = "";
  if ($display_blocks == 1) {
    $out = format_ascii_color_as_block($color);
  }
  return $out;
}

sub format_ascii_color_text {
  my $color = $_[0];
  my $out = "";
  if ($display_hex == 1) {
    #$out = $out . $ascii_color_hex_table[$decimal_value];
    $out = format_ascii_color_as_hex($color);
  }
  if ($out eq "") {
    $out = format_ascii_color_as_dec($color);
  }
  if ($display_verbose == 1) {
    $out = format_ascii_color_as_dec($color) . '| ' . $out;
  }
  return $out;
}

sub format_ascii_color {
  my $color = $_[0];
  my $out = "";
  if ($display_blocks == 1) {
    $out = format_ascii_color_block($color);
    if ($display_verbose == 1) {
      $out = format_ascii_color_as_dec($color) . '| ' . $out;
    }
  }
  else {
    $out = format_ascii_color_text($color);
  }
  return $out;
}

sub format_ascii_color_as_block {
  my $block = "";
  # we want to ignore verbose if it's on
  my $verbose_backup = $display_verbose;
  $display_verbose = 0;
  my $width = get_column_width();
  $display_verbose = $verbose_backup;
  for(my $idx = 0; $idx < $width; $idx++) {
    $block .= sprintf "\x{2588}";
  }
  return $block;
}

sub format_ascii_color_as_dec {
  my $width   = 1;

  if ($display_extended == 1) {
    $width = 3;
  }
  elsif ($display_verbose == 1) {
    $width = 3;
  }
  elsif ($display_tabular == 1) {
    $width = 2;
  }

  return sprintf("%0*d", $width, $_[0]);
}

sub format_ascii_color_as_hex {
  my $width = 6;
  return sprintf("%*s", $width, $ascii_color_hex_table[$_[0]]);
}

#{{{ Data
push(@ascii_color_hex_table, "000000"); 
push(@ascii_color_hex_table, "800000"); 
push(@ascii_color_hex_table, "008000"); 
push(@ascii_color_hex_table, "808000"); 
push(@ascii_color_hex_table, "000080"); 
push(@ascii_color_hex_table, "800080"); 
push(@ascii_color_hex_table, "008080"); 
push(@ascii_color_hex_table, "c0c0c0"); 
push(@ascii_color_hex_table, "808080"); 
push(@ascii_color_hex_table, "ff0000"); 
push(@ascii_color_hex_table, "00ff00"); 
push(@ascii_color_hex_table, "ffff00"); 
push(@ascii_color_hex_table, "0000ff"); 
push(@ascii_color_hex_table, "ff00ff"); 
push(@ascii_color_hex_table, "00ffff"); 
push(@ascii_color_hex_table, "ffffff"); 

push(@ascii_color_hex_table, "000000"); 
push(@ascii_color_hex_table, "00005f"); 
push(@ascii_color_hex_table, "000087"); 
push(@ascii_color_hex_table, "0000af"); 
push(@ascii_color_hex_table, "0000d7"); 
push(@ascii_color_hex_table, "0000ff"); 
push(@ascii_color_hex_table, "005f00"); 
push(@ascii_color_hex_table, "005f5f"); 
push(@ascii_color_hex_table, "005f87"); 
push(@ascii_color_hex_table, "005faf"); 
push(@ascii_color_hex_table, "005fd7"); 
push(@ascii_color_hex_table, "005fff"); 
push(@ascii_color_hex_table, "008700"); 
push(@ascii_color_hex_table, "00875f"); 
push(@ascii_color_hex_table, "008787"); 
push(@ascii_color_hex_table, "0087af"); 
push(@ascii_color_hex_table, "0087d7"); 
push(@ascii_color_hex_table, "0087ff"); 
push(@ascii_color_hex_table, "00af00"); 
push(@ascii_color_hex_table, "00af5f"); 
push(@ascii_color_hex_table, "00af87"); 
push(@ascii_color_hex_table, "00afaf"); 
push(@ascii_color_hex_table, "00afd7"); 
push(@ascii_color_hex_table, "00afff"); 
push(@ascii_color_hex_table, "00d700"); 
push(@ascii_color_hex_table, "00d75f"); 
push(@ascii_color_hex_table, "00d787"); 
push(@ascii_color_hex_table, "00d7af"); 
push(@ascii_color_hex_table, "00d7d7"); 
push(@ascii_color_hex_table, "00d7ff"); 
push(@ascii_color_hex_table, "00ff00"); 
push(@ascii_color_hex_table, "00ff5f"); 
push(@ascii_color_hex_table, "00ff87"); 
push(@ascii_color_hex_table, "00ffaf"); 
push(@ascii_color_hex_table, "00ffd7"); 
push(@ascii_color_hex_table, "00ffff"); 
push(@ascii_color_hex_table, "5f0000"); 
push(@ascii_color_hex_table, "5f005f"); 
push(@ascii_color_hex_table, "5f0087"); 
push(@ascii_color_hex_table, "5f00af"); 
push(@ascii_color_hex_table, "5f00d7"); 
push(@ascii_color_hex_table, "5f00ff"); 
push(@ascii_color_hex_table, "5f5f00"); 
push(@ascii_color_hex_table, "5f5f5f"); 
push(@ascii_color_hex_table, "5f5f87"); 
push(@ascii_color_hex_table, "5f5faf"); 
push(@ascii_color_hex_table, "5f5fd7"); 
push(@ascii_color_hex_table, "5f5fff"); 
push(@ascii_color_hex_table, "5f8700"); 
push(@ascii_color_hex_table, "5f875f"); 
push(@ascii_color_hex_table, "5f8787"); 
push(@ascii_color_hex_table, "5f87af"); 
push(@ascii_color_hex_table, "5f87d7"); 
push(@ascii_color_hex_table, "5f87ff"); 
push(@ascii_color_hex_table, "5faf00"); 
push(@ascii_color_hex_table, "5faf5f"); 
push(@ascii_color_hex_table, "5faf87"); 
push(@ascii_color_hex_table, "5fafaf"); 
push(@ascii_color_hex_table, "5fafd7"); 
push(@ascii_color_hex_table, "5fafff"); 
push(@ascii_color_hex_table, "5fd700"); 
push(@ascii_color_hex_table, "5fd75f"); 
push(@ascii_color_hex_table, "5fd787"); 
push(@ascii_color_hex_table, "5fd7af"); 
push(@ascii_color_hex_table, "5fd7d7"); 
push(@ascii_color_hex_table, "5fd7ff"); 
push(@ascii_color_hex_table, "5fff00"); 
push(@ascii_color_hex_table, "5fff5f"); 
push(@ascii_color_hex_table, "5fff87"); 
push(@ascii_color_hex_table, "5fffaf"); 
push(@ascii_color_hex_table, "5fffd7"); 
push(@ascii_color_hex_table, "5fffff"); 
push(@ascii_color_hex_table, "870000"); 
push(@ascii_color_hex_table, "87005f"); 
push(@ascii_color_hex_table, "870087"); 
push(@ascii_color_hex_table, "8700af"); 
push(@ascii_color_hex_table, "8700d7"); 
push(@ascii_color_hex_table, "8700ff"); 
push(@ascii_color_hex_table, "875f00"); 
push(@ascii_color_hex_table, "875f5f"); 
push(@ascii_color_hex_table, "875f87"); 
push(@ascii_color_hex_table, "875faf"); 
push(@ascii_color_hex_table, "875fd7"); 
push(@ascii_color_hex_table, "875fff"); 
push(@ascii_color_hex_table, "878700"); 
push(@ascii_color_hex_table, "87875f"); 
push(@ascii_color_hex_table, "878787"); 
push(@ascii_color_hex_table, "8787af"); 
push(@ascii_color_hex_table, "8787d7"); 
push(@ascii_color_hex_table, "8787ff"); 
push(@ascii_color_hex_table, "87af00"); 
push(@ascii_color_hex_table, "87af5f"); 
push(@ascii_color_hex_table, "87af87"); 
push(@ascii_color_hex_table, "87afaf"); 
push(@ascii_color_hex_table, "87afd7"); 
push(@ascii_color_hex_table, "87afff"); 
push(@ascii_color_hex_table, "87d700"); 
push(@ascii_color_hex_table, "87d75f"); 
push(@ascii_color_hex_table, "87d787"); 
push(@ascii_color_hex_table, "87d7af"); 
push(@ascii_color_hex_table, "87d7d7"); 
push(@ascii_color_hex_table, "87d7ff"); 
push(@ascii_color_hex_table, "87ff00"); 
push(@ascii_color_hex_table, "87ff5f"); 
push(@ascii_color_hex_table, "87ff87"); 
push(@ascii_color_hex_table, "87ffaf"); 
push(@ascii_color_hex_table, "87ffd7"); 
push(@ascii_color_hex_table, "87ffff"); 
push(@ascii_color_hex_table, "af0000"); 
push(@ascii_color_hex_table, "af005f"); 
push(@ascii_color_hex_table, "af0087"); 
push(@ascii_color_hex_table, "af00af"); 
push(@ascii_color_hex_table, "af00d7"); 
push(@ascii_color_hex_table, "af00ff"); 
push(@ascii_color_hex_table, "af5f00"); 
push(@ascii_color_hex_table, "af5f5f"); 
push(@ascii_color_hex_table, "af5f87"); 
push(@ascii_color_hex_table, "af5faf"); 
push(@ascii_color_hex_table, "af5fd7"); 
push(@ascii_color_hex_table, "af5fff"); 
push(@ascii_color_hex_table, "af8700"); 
push(@ascii_color_hex_table, "af875f"); 
push(@ascii_color_hex_table, "af8787"); 
push(@ascii_color_hex_table, "af87af"); 
push(@ascii_color_hex_table, "af87d7"); 
push(@ascii_color_hex_table, "af87ff"); 
push(@ascii_color_hex_table, "afaf00"); 
push(@ascii_color_hex_table, "afaf5f"); 
push(@ascii_color_hex_table, "afaf87"); 
push(@ascii_color_hex_table, "afafaf"); 
push(@ascii_color_hex_table, "afafd7"); 
push(@ascii_color_hex_table, "afafff"); 
push(@ascii_color_hex_table, "afd700"); 
push(@ascii_color_hex_table, "afd75f"); 
push(@ascii_color_hex_table, "afd787"); 
push(@ascii_color_hex_table, "afd7af"); 
push(@ascii_color_hex_table, "afd7d7"); 
push(@ascii_color_hex_table, "afd7ff"); 
push(@ascii_color_hex_table, "afff00"); 
push(@ascii_color_hex_table, "afff5f"); 
push(@ascii_color_hex_table, "afff87"); 
push(@ascii_color_hex_table, "afffaf"); 
push(@ascii_color_hex_table, "afffd7"); 
push(@ascii_color_hex_table, "afffff"); 
push(@ascii_color_hex_table, "d70000"); 
push(@ascii_color_hex_table, "d7005f"); 
push(@ascii_color_hex_table, "d70087"); 
push(@ascii_color_hex_table, "d700af"); 
push(@ascii_color_hex_table, "d700d7"); 
push(@ascii_color_hex_table, "d700ff"); 
push(@ascii_color_hex_table, "d75f00"); 
push(@ascii_color_hex_table, "d75f5f"); 
push(@ascii_color_hex_table, "d75f87"); 
push(@ascii_color_hex_table, "d75faf"); 
push(@ascii_color_hex_table, "d75fd7"); 
push(@ascii_color_hex_table, "d75fff"); 
push(@ascii_color_hex_table, "d78700"); 
push(@ascii_color_hex_table, "d7875f"); 
push(@ascii_color_hex_table, "d78787"); 
push(@ascii_color_hex_table, "d787af"); 
push(@ascii_color_hex_table, "d787d7"); 
push(@ascii_color_hex_table, "d787ff"); 
push(@ascii_color_hex_table, "d7af00"); 
push(@ascii_color_hex_table, "d7af5f"); 
push(@ascii_color_hex_table, "d7af87"); 
push(@ascii_color_hex_table, "d7afaf"); 
push(@ascii_color_hex_table, "d7afd7"); 
push(@ascii_color_hex_table, "d7afff"); 
push(@ascii_color_hex_table, "d7d700"); 
push(@ascii_color_hex_table, "d7d75f"); 
push(@ascii_color_hex_table, "d7d787"); 
push(@ascii_color_hex_table, "d7d7af"); 
push(@ascii_color_hex_table, "d7d7d7"); 
push(@ascii_color_hex_table, "d7d7ff"); 
push(@ascii_color_hex_table, "d7ff00"); 
push(@ascii_color_hex_table, "d7ff5f"); 
push(@ascii_color_hex_table, "d7ff87"); 
push(@ascii_color_hex_table, "d7ffaf"); 
push(@ascii_color_hex_table, "d7ffd7"); 
push(@ascii_color_hex_table, "d7ffff"); 
push(@ascii_color_hex_table, "ff0000"); 
push(@ascii_color_hex_table, "ff005f"); 
push(@ascii_color_hex_table, "ff0087"); 
push(@ascii_color_hex_table, "ff00af"); 
push(@ascii_color_hex_table, "ff00d7"); 
push(@ascii_color_hex_table, "ff00ff"); 
push(@ascii_color_hex_table, "ff5f00"); 
push(@ascii_color_hex_table, "ff5f5f"); 
push(@ascii_color_hex_table, "ff5f87"); 
push(@ascii_color_hex_table, "ff5faf"); 
push(@ascii_color_hex_table, "ff5fd7"); 
push(@ascii_color_hex_table, "ff5fff"); 
push(@ascii_color_hex_table, "ff8700"); 
push(@ascii_color_hex_table, "ff875f"); 
push(@ascii_color_hex_table, "ff8787"); 
push(@ascii_color_hex_table, "ff87af"); 
push(@ascii_color_hex_table, "ff87d7"); 
push(@ascii_color_hex_table, "ff87ff"); 
push(@ascii_color_hex_table, "ffaf00"); 
push(@ascii_color_hex_table, "ffaf5f"); 
push(@ascii_color_hex_table, "ffaf87"); 
push(@ascii_color_hex_table, "ffafaf"); 
push(@ascii_color_hex_table, "ffafd7"); 
push(@ascii_color_hex_table, "ffafff"); 
push(@ascii_color_hex_table, "ffd700"); 
push(@ascii_color_hex_table, "ffd75f"); 
push(@ascii_color_hex_table, "ffd787"); 
push(@ascii_color_hex_table, "ffd7af"); 
push(@ascii_color_hex_table, "ffd7d7"); 
push(@ascii_color_hex_table, "ffd7ff"); 
push(@ascii_color_hex_table, "ffff00"); 
push(@ascii_color_hex_table, "ffff5f"); 
push(@ascii_color_hex_table, "ffff87"); 
push(@ascii_color_hex_table, "ffffaf"); 
push(@ascii_color_hex_table, "ffffd7"); 
push(@ascii_color_hex_table, "ffffff"); 
push(@ascii_color_hex_table, "080808"); 
push(@ascii_color_hex_table, "121212"); 
push(@ascii_color_hex_table, "1c1c1c"); 
push(@ascii_color_hex_table, "262626"); 
push(@ascii_color_hex_table, "303030"); 
push(@ascii_color_hex_table, "3a3a3a"); 
push(@ascii_color_hex_table, "444444"); 
push(@ascii_color_hex_table, "4e4e4e"); 
push(@ascii_color_hex_table, "585858"); 
push(@ascii_color_hex_table, "626262"); 
push(@ascii_color_hex_table, "6c6c6c"); 
push(@ascii_color_hex_table, "767676"); 
push(@ascii_color_hex_table, "808080"); 
push(@ascii_color_hex_table, "8a8a8a"); 
push(@ascii_color_hex_table, "949494"); 
push(@ascii_color_hex_table, "9e9e9e"); 
push(@ascii_color_hex_table, "a8a8a8"); 
push(@ascii_color_hex_table, "b2b2b2"); 
push(@ascii_color_hex_table, "bcbcbc"); 
push(@ascii_color_hex_table, "c6c6c6"); 
push(@ascii_color_hex_table, "d0d0d0"); 
push(@ascii_color_hex_table, "dadada"); 
push(@ascii_color_hex_table, "e4e4e4"); 
push(@ascii_color_hex_table, "eeeeee"); 
#}}} End Data

@ascii_color_name_table = (
  { "short" => "bk", "long" => "black" },
  { "short" => "rd", "long" => "red" },
  { "short" => "gr", "long" => "green" },
  { "short" => "yw", "long" => "yellow" },
  { "short" => "bl", "long" => "blue" },
  { "short" => "ma", "long" => "magenta" },
  { "short" => "cy", "long" => "cyan" },
  { "short" => "wh", "long" => "white" },
);

sub get_total_column_width {
  my $width               = get_column_width();
  my $padding_left_width  = get_column_padding_left_width();
  my $padding_right_width = get_column_padding_right_width();

  $width = $width +  $padding_right_width + $padding_left_width;
  return $width;
}
sub get_column_width {
  #print "width =1\n";
  my $width = 1;
  if ($display_wide == 1)
  { 
    if ($display_blocks == 1) {
      #print "width2 =6\n";
      $width = 6; 
    }
    else {
      #print "width +6\n";
      $width += 6; 
    }
  }
  elsif ($display_tabular == 1) { 
    if ($display_extended == 1) {
      #print "width +5\n";
      $width += 5;
    }
  }
  if ($display_hex == 1) {
    if ($width < 6) {
      #print "width1 =6\n";
      $width = 6;
    }
  }
  if ($display_verbose == 1) {
    #print "width +5\n";
    $width += 5;
  }
  if ($width % 2 != 0) {
    #print "width +1\n";
    $width += 1;
  }
  #print "\ncolumn width: $width\n";
  return $width;
}

sub get_sub_header_column_width {
  return 8;
}

sub render_chart_sub_header {
  my $out = "";
  if($_[0] =~ /^-?\d+$/) {
    $out = format_ascii_color_as_dec($_[0]);
  }
  elsif($_[0] ne "") {
    $out = "$_[0] >"
  }
  sprintf("%*s", get_sub_header_column_width(), "$out");
}

sub render_chart_header {
  return sprintf("\n%s:\n\n", $_[0]);
}

sub print_ascii_colors {
  my $idx = 0;
  my $columns = get_column_count();
  print render_chart_header("ASCII Colors");

  # print column headers if verbose and we are in tabular format
  if ($display_tabular == 1) {
    if ($display_verbose == 1) {
      printf("%*s", get_sub_header_column_width(), "");
    }
    for( $idx = 0; $idx < 8; $idx++ ) {
      print render_ascii_color_name($idx);
    }
    print "\n";
  }

  for( $idx = 0; $idx < 16; $idx++ ) {
    if ($display_verbose == 1 && $columns >= 8 && $idx == 0) {
      print render_chart_sub_header("Normal");
    }
    elsif ($display_verbose == 1 && $columns >= 8 && $idx == 8) {
      print render_chart_sub_header("Bold");
    }

    print render_ascii_color($idx);

    if(($idx + 1) % $columns == 0) {
      print "\n";
    }
  }

  if ($display_extended == 1) {
    print_ascii_colors_extended();
    print_ascii_colors_grey_scale();
  }
}

sub print_ascii_colors_extended {
  # offset to start in the extended range
  my $idx     = 16;
  my $idx_max = 232;
  my $idx_col = 0;
  my $idx_row = 0;
  my $cols = get_column_count();
  # override this value if we are in tabular mode
  if ($display_tabular == 1) { $cols = 6 }
  my $rows = ($idx_max - $idx) / $cols;
  print render_chart_header("ASCII 256 Colors");

  my $out = "";
  my $color = 0;

  for($idx_row = 0; $idx_row < $rows; $idx_row++) {
    print render_chart_sub_header("");
    for($idx_col = 0; $idx_col < $cols; $idx_col++) {
      #$color = ($idx_row + ($rows * $idx_col) + 16);
      $color = (($idx_row * $cols ) + $idx_col + 16);
      $out = sprintf "%s[%d]", "$idx_col,$idx_row", $color;
      print render_ascii_color($color);
      #printf "%*s", get_column_width(), $out;

      $idx++;
      if ($idx >= $idx_max) { print "\n"; return; }
    }
    print "\n";
  }


}

sub print_ascii_colors_grey_scale {
  # offset to start in the extended range
  my $idx     = 232;
  my $idx_max = 256;
  my $idx_col = 0;
  my $idx_row = 0;
  my $cols = get_column_count();
  # override this value if we are in tabular mode
  if ($display_tabular == 1) { $cols = 6 }
  my $rows = ($idx_max - $idx) / $cols;
  print render_chart_header("ASCII 256 Colors - Grey Scale - $idx_row -> $rows / $idx_max");

  my $out = "";
  my $color = 0;

  for($idx_row = 232; $idx_row < $idx_max; $idx_row++) {
    print render_chart_sub_header("");
    for($idx_col = 0; $idx_col < $cols; $idx_col++) {
      #print "col: $idx_col row: $idx_row => \n";
      #$color = ($idx_row + ($rows * $idx_col) + 16);
      $color = $idx; 
      $out = sprintf "%s[%d]", "$idx_col,$idx_row", $color;
      print render_ascii_color($color);
      #printf "**%*s**", get_column_width(), $out;

      $idx++;
      if ($idx >= $idx_max) { return; }
    }
    print "\n";
  }

}

print_ascii_colors();

print "\n";
print "\n";


exit;
