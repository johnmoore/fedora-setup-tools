#!/bin/bash

##############################################################################
# Config 
##############################################################################

# set variables
file=Terminal.desktop
file_path=/usr/share/applications
yum_opts="-y"

##############################################################################
# Script
##############################################################################

# must be run as root
if [ $(whoami) != "root" ]; then
	echo "[ee]: script must be run as root"
	exit 1
fi

# check if a backup file exists
if [ -f "$file_path/$file.bak" ]; then
	# remove the backup file
	rm "$file_path/$file.bak"
fi

# uninstall terminal

yum "$yum_opts" remove Terminal

exit 0
